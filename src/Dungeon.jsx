// @flow

import React, { PropTypes, Component } from 'react';

import { Container, Button } from 'muicss/react';

import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import Location from './Location.jsx';

import './App.css';
import './main.css';


type State = {
  dungeonMap: Array<Object>,
  rooms: Array<Object>,
  cols: number,
  rows: number,
  player: Object,
  boss: Object,
  enemies: Array<Object>,
};

type Props = {
  darkness: boolean,
  handleToggleDarkness: Function,
};


class Dungeon extends Component<void, Props, State> {
  state: State;

  setupMap: Function;
  createRoom: Function;
  connectRooms: Function;
  createEnemies: Function;
  createHealthItems: Function;
  createWeaponUpgrades: Function;
  findEmptyLocation: Function;
  attackLocation: Function;
  handlePlayerMove: Function;
  handleMyToggleDarkness: Function;

  constructor(props: Props) {
    super(props);

    // console.log(`constructor props: ${JSON.stringify(props)}`);

    this.setupMap = this.setupMap.bind(this);
    this.createRoom = this.createRoom.bind(this);
    this.connectRooms = this.connectRooms.bind(this);
    this.createEnemies = this.createEnemies.bind(this);
    this.createHealthItems = this.createHealthItems.bind(this);
    this.createWeaponUpgrades = this.createWeaponUpgrades.bind(this);
    this.findEmptyLocation = this.findEmptyLocation.bind(this);
    this.attackLocation = this.attackLocation.bind(this);
    this.handlePlayerMove = this.handlePlayerMove.bind(this);

    this.handleMyToggleDarkness = this.handleMyToggleDarkness.bind(this);
    // this.handleToggleDarkness = this.handleToggleDarkness.bind(this);

    this.state = this.setDefaultState();
  }

  // This is a shim to be able to attach flow annotations
  // and enforce type safety in calls to `setState` within
  // this component.
  /*
  setState(object: State) {
    super.setState(object);
  }
  */

  // move into reducer
  setDefaultState() {
    const MAPSIZE = 60;

    return (
    {
      dungeonMap: [],
      rooms: [],
      cols: MAPSIZE,
      rows: MAPSIZE,
      player: {
        health: 100,
        level: 1,
        nextLevel: 20,
        experience: 1,
        damage: {
          min: 5,
          max: 10,
        },
      },
      boss: {
        health: 50,
        damage: {
          min: 20,
          max: 20,
        },
        experienceIncrease: 50,
      },
      enemies: [],
    }
    );
  }


  handleMyToggleDarkness() {
    const { handleToggleDarkness } = this.props;
    handleToggleDarkness();

    // mrc
    // test removing this code after finishing the move to redux
    this.forceUpdate();
    /*
    this.setState({
    }, () => {
      this.forceUpdate();
    });
    */
  }


  componentWillMount() {
    this.setupMap();
    document.addEventListener('keydown', this.handlePlayerMove, false);
  }

  componentDidMount() {
    this.notifyStartOfGame();
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log(`nextprops: ${JSON.stringify(nextProps)}`);
  // }

  notifyStartOfGame(): void {
    NotificationManager.error('Starting new game....');
  }

  calcMiddle(start: number, end: number): number {
    return (Math.floor(end / 2) + start);
  }

  getRandomInt(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  createRoom(dungeonLocations: Object): Object {
    const newDungeonLocations: Object = dungeonLocations.map((arr) => arr.slice());

    const roomCols = this.getRandomInt(4, 4 + parseInt(this.state.cols * 0.25, 10));
    const roomRows = this.getRandomInt(4, 4 + parseInt(this.state.rows * 0.25, 10));

    const roomX = this.getRandomInt(1, this.state.cols - roomCols);
    const roomY = this.getRandomInt(1, this.state.rows - roomRows);


    for (let y = roomY; y < roomY + roomRows; y++) {
      for (let x = roomX; x < roomX + roomCols; x++) {
        if (newDungeonLocations[y][x] === 'empty') {
          return {
            arr: newDungeonLocations,
            cnt: 0,
          };
        }
        newDungeonLocations[y][x] = 'empty';
      }
    }

    this.state.rooms.push({
      roomX,
      roomY,
      roomCols,
      roomRows,
    });
    return {
      arr: newDungeonLocations,
      cnt: roomCols * roomRows,
    };
  }

  connectRooms(dungeonLocations: Object): Object {
    const newDungeonLocations: Object = dungeonLocations.map((arr) => arr.slice());
    this.state.rooms.forEach((element, index, array) => {
      if (index + 1 < array.length) {
        let newCol;

        const currentRoomMiddleX = this.calcMiddle(element.roomX, element.roomCols);
        const nextRoomMiddleX = this.calcMiddle(array[index + 1].roomX, array[index + 1].roomCols);
        const currentRoomMiddleY = this.calcMiddle(element.roomY, element.roomRows);
        const nextRoomMiddleY = this.calcMiddle(array[index + 1].roomY, array[index + 1].roomRows);

        const xLocationDiff = Math.abs(currentRoomMiddleX - nextRoomMiddleX);
        const yLocationDiff = Math.abs(currentRoomMiddleY - nextRoomMiddleY);

        const startPointX = currentRoomMiddleX < nextRoomMiddleX
          ? currentRoomMiddleX
          : nextRoomMiddleX;
        const startPointY = currentRoomMiddleY < nextRoomMiddleY
          ? currentRoomMiddleY
          : nextRoomMiddleY;

        for (let i = startPointX; i <= xLocationDiff + startPointX; i++) {
          newDungeonLocations[startPointY][i] = 'empty';
        }

        if (((currentRoomMiddleX < nextRoomMiddleX) &&
            (currentRoomMiddleY > nextRoomMiddleY)) ||
           ((currentRoomMiddleX > nextRoomMiddleX) &&
            (currentRoomMiddleY > nextRoomMiddleY))) {
          newCol = currentRoomMiddleX;
        }
        else {
          newCol = nextRoomMiddleX;
        }
        for (let i = startPointY; i <= yLocationDiff + startPointY; i++) {
          newDungeonLocations[i][newCol] = 'empty';
        }
      }
    });
    return newDungeonLocations;
  }

  createEnemies(dungeonLocations: Object): Object {
    const newDungeonLocations: Object = dungeonLocations.map((arr) => arr.slice());
    const findEmptyLocation = this.findEmptyLocation(dungeonLocations);
    newDungeonLocations[findEmptyLocation.y][findEmptyLocation.x] = 'enemy';
    this.state.enemies.push({
      x: findEmptyLocation.x,
      y: findEmptyLocation.y,
      health: 15,
      damage: { min: 3, max: 8 },
      experienceIncrease: 8,
    });
    return newDungeonLocations;
  }

  createHealthItems(dungeonLocations: Object): Object {
    const newDungeonLocations: Object = dungeonLocations.map((arr) => arr.slice());
    const findEmptyLocation = this.findEmptyLocation(dungeonLocations);
    newDungeonLocations[findEmptyLocation.y][findEmptyLocation.x] = 'health';
    return newDungeonLocations;
  }

  createWeaponUpgrades(dungeonLocations: Object): Object {
    const newDungeonLocations: Object = dungeonLocations.map((arr) => arr.slice());
    const findEmptyLocation = this.findEmptyLocation(dungeonLocations);
    newDungeonLocations[findEmptyLocation.y][findEmptyLocation.x] = 'weapon';
    return newDungeonLocations;
  }

  findEmptyLocation(dungeonLocations: Array<Array<string>>): {x: number, y: number} {
    let x = 0;
    let y = 0;
    const i = true;
    while (i) {
      x = Math.floor(Math.random() * this.state.cols);
      y = Math.floor(Math.random() * this.state.rows);
      if (dungeonLocations[y][x] === 'empty') {
        return { x, y };
      }
    }
    return { x, y };
  }

  createWalls(): Array<Object> {
    const newDungeonLocations: any = [];
    // set up map area with all locations as walls
    for (let x = 0; x < this.state.rows; x++) {
      let tmpGridRows = [];
      for (let y = 0; y < this.state.cols; y++) {
        tmpGridRows.push('wall');
      }
      newDungeonLocations.push(tmpGridRows);
      tmpGridRows = [];
    }

    return newDungeonLocations;
  }

  createRooms(dungeonLocations: Array<Object>): Array<Object> {
    let newDungeonLocations = dungeonLocations.map((arr) => arr.slice());

    // carve out the rooms from the map
    for (let x = 0; x < this.state.rows; x++) {
      let empty = 0;
      const tmpRoom = this.createRoom(newDungeonLocations);
      newDungeonLocations = tmpRoom.arr;
      empty += tmpRoom.cnt;
      if ((empty / (this.state.cols * this.state.rows)) > 0.33) {
        break;
      }
    }

    return newDungeonLocations;
  }

  setPlayerStartLocation(dungeonLocations: any): Array<Object> {
    const newDungeonLocations = dungeonLocations.map((arr) => arr.slice());

    // create player in the middle of the first room
    newDungeonLocations[this.calcMiddle(this.state.rooms[0].roomY, this.state.rooms[0].roomRows)][this.calcMiddle(this.state.rooms[0].roomX, this.state.rooms[0].roomCols)] = 'player';

    const player = this.state.player;
    player.y = this.calcMiddle(this.state.rooms[0].roomY, this.state.rooms[0].roomRows);
    player.x = this.calcMiddle(this.state.rooms[0].roomX, this.state.rooms[0].roomCols);

    // mrc
    this.setState({ player });

    return newDungeonLocations;
  }

  setBossStartLocation(dungeonLocations: any): Array<Object> {
    const newDungeonLocations = dungeonLocations.map((arr) => arr.slice());

    // create boss and place in the middle of the last room created
    const lastRoomNum = this.state.rooms.length - 1;
    const boss = this.state.boss;
    const lastRoomY =
      this.calcMiddle(this.state.rooms[lastRoomNum].roomY,
      this.state.rooms[lastRoomNum].roomRows);
    const lastRoomX =
      this.calcMiddle(this.state.rooms[lastRoomNum].roomX,
      this.state.rooms[lastRoomNum].roomCols);

    newDungeonLocations[lastRoomY][lastRoomX] = 'boss';
    boss.x = lastRoomX;
    boss.y = lastRoomY;
    // mrc
    this.setState({ boss });
    this.state.enemies.push(boss);

    return newDungeonLocations;
  }

  setupMap(): void {
    let x = 0;
    let dungeonLocations = this.createWalls();

    dungeonLocations = this.createRooms(dungeonLocations);

    // create hallways between the rooms
    dungeonLocations = this.connectRooms(dungeonLocations);

    dungeonLocations = this.setPlayerStartLocation(dungeonLocations);

    // create enemies
    const numberOfEnemies = 30;
    for (x = 0; x < numberOfEnemies; x++) {
      dungeonLocations = this.createEnemies(dungeonLocations);
    }

    // create health items
    const numberOfHealth = 6;
    for (x = 0; x < numberOfHealth; x++) {
      dungeonLocations = this.createHealthItems(dungeonLocations);
    }

    // create weapon upgrades
    const numberOfWeaponUpgrades = 2;
    for (x = 0; x < numberOfWeaponUpgrades; x++) {
      dungeonLocations = this.createWeaponUpgrades(dungeonLocations);
    }

    dungeonLocations = this.setBossStartLocation(dungeonLocations);


    // finalize initial map
    // mrc
    this.setState({
      dungeonMap: dungeonLocations,
    });
  }


  resetPlayers(): void {
    const player = this.state.player;
    player.health = 100;
    player.level = 1;
    player.nextLevel = 20;
    player.experience = 1;
    player.damage = {
      min: 5,
      max: 10,
    };

    this.setState({ player });


    const boss = this.state.boss;
    boss.health = 50;

    this.setState({ boss });
  }

  setNewGame(): void {
    this.resetPlayers();

    this.setupMap();
    this.notifyStartOfGame();
  }


  attackLocation(enemyY: number, enemyX: number): boolean {
    const playerDamage = Math.floor((Math.random() *
      (this.state.player.damage.max - this.state.player.damage.min))
      + this.state.player.damage.min);
    let newDungeonMap: Array<Object>;
    let enemyDamage: number;
    const player = this.state.player;
    this.state.enemies.find((tempEnemy) => {
      const currentEnemy = tempEnemy;

      if (currentEnemy.x === enemyX && currentEnemy.y === enemyY) {
        currentEnemy.health -= playerDamage;
        if (currentEnemy.health < 0) {
          if (this.state.dungeonMap[enemyY][enemyX] === 'boss') {
            NotificationManager.success('Boss Defeated. YOU WIN!');

            this.setNewGame();
            return (true);
          }

          NotificationManager.success('Enemy destroyed');
          newDungeonMap = this.state.dungeonMap.slice();
          newDungeonMap[enemyY][enemyX] = 'empty';
          // mrc
          this.setState({ dungeonMap: newDungeonMap });
          player.experience += currentEnemy.experienceIncrease;
        }
        enemyDamage = Math.floor((Math.random() *
          (currentEnemy.damage.max - currentEnemy.damage.min)) +
          currentEnemy.damage.min);

        player.health -= enemyDamage;

        if (player.health <= 0) {
          NotificationManager.error('GAME OVER. YOU LOSE!!');

          this.setNewGame();
          return (true);
        }

        if (player.experience >= player.nextLevel) {
          player.level += 1;
          NotificationManager.success(`Leveled Up! You are now level ${player.level}`);
          player.nextLevel += Math.floor(player.nextLevel * 1.25);

          if (player.health < 100) {
            player.health = 100;
          }
          NotificationManager.info(`Health  ${player.health}%`);
          player.damage.min += 1;
          player.damage.max += 1;
        }
        else {
          NotificationManager.error(`Health  ${player.health}%`);
        }

        // mrc
        this.setState({ player });
      }
      return (false);
    });
    return (false);
  }


  setNewLocation(newY: number, newX: number): void {
    const newDungeonMap = this.state.dungeonMap.slice();
    const player = this.state.player;
    newDungeonMap[newY][newX] = 'player';
    player.y = newY;
    player.x = newX;
    this.setState({ player });
    this.setState({ dungeonMap: newDungeonMap });
  }
  setOldLocation(prevY: number, prevX: number): void {
    const newDungeonMap = this.state.dungeonMap.slice();
    newDungeonMap[prevY][prevX] = 'empty';
    this.setState({ dungeonMap: newDungeonMap });
  }

  upgradeHealth(): void {
    const player = this.state.player;
    player.health += 10;

    NotificationManager.info(`Found a health potion. Health ${player.health}%`);
    this.setState({ player });
  }

  upgradeWeapon(): void {
    const player = this.state.player;

    player.damage.min += 3;
    player.damage.max += 3;

    this.setState({ player });
  }

  move(newY: number, newX: number, prevY: number, prevX: number): void  {
    switch (this.state.dungeonMap[newY][newX]) {
      case 'boss':
        this.attackLocation(newY, newX);
        break;
      case 'enemy':
        this.attackLocation(newY, newX);
        break;
      case 'wall':
        break;
      case 'weapon':
        this.upgradeWeapon();
        NotificationManager.info('Improved weapon damage');

        this.setOldLocation(prevY, prevX);
        this.setNewLocation(newY, newX);

        break;
      case 'health':
        this.upgradeHealth();

        this.setOldLocation(prevY, prevX);
        this.setNewLocation(newY, newX);

        break;
      default:
        // move to empty space
        this.setOldLocation(prevY, prevX);
        this.setNewLocation(newY, newX);

        break;
    }
  }

  handleKeyBoardEvent(event: Object): void {
    const player = this.state.player;
    switch (event.keyCode) {
      case 37: // left
      case 65: // A
        this.move(player.y, player.x - 1, player.y, player.x);
        break;
      case 38: // up
      case 87: // w
        this.move(player.y - 1, player.x, player.y, player.x);
        break;
      case 39: // right
      case 68: // d
        this.move(player.y, player.x + 1, player.y, player.x);
        break;
      case 40: // down
      case 83: // s
        this.move(player.y + 1, player.x, player.y, player.x);
        break;
      default:
        break;
    }
  }

  handlePlayerMove(event: Object): void {
    this.handleKeyBoardEvent(event);
  }


  render(): Object {
    // console.log(`props: ${JSON.stringify(this.props)}`);

    // size of the visible map
    const viewPortX = 10;
    const viewPortY = 8;

    let locations = [];
    const locationRows = [];
    const playerX: number = this.state.player.x;
    const playerY:  number = this.state.player.y;

    for (let y = 0; y < this.state.rows; y++) {
      let viewXdist = 0;
      let viewYdist = 0;

      const yLocationDiff = y - playerY;
      const yDiffabs = Math.abs(yLocationDiff);
      if (yLocationDiff < 0 && playerY < viewPortY) {
        // top side of player
        viewYdist = viewPortY - playerY - 1;
      }
      else if (playerY >= this.state.rows - viewPortY) {
        // bottom side of player
        viewYdist = viewPortY - (this.state.rows - playerY);
      }
      if (Math.abs(yLocationDiff) < viewPortY + viewYdist) {
        for (let x = 0; x < this.state.cols; x++) {
          const xLocationDiff = x - playerX;
          const xDiffabs = Math.abs(xLocationDiff);
          if (xLocationDiff < 0 && playerX < viewPortX) {
            // left side of player
            viewXdist = viewPortX - playerX;
          }
          else if (playerX >= this.state.cols - viewPortX) {
            // right side of player
            viewXdist = viewPortX - ((this.state.cols - playerX) + 1);
          }
          if (xDiffabs <= viewPortX + viewXdist) {
            let locationType = this.state.dungeonMap[y][x];

            // console.log(`props: ${JSON.stringify(this.props.darkness)}`);
            if (this.props.darkness) {
              if ((xDiffabs === 5 && yDiffabs === 5) ||
                 (xDiffabs === 4 && yDiffabs === 5) ||
                 (xDiffabs === 5 && yDiffabs === 4) ||
                 (xDiffabs > 5 && yDiffabs > 5) ||
                  xDiffabs > 5 ||
                  yDiffabs > 5) {
                locationType = 'darkness';
              }
            }
            // console.log(`props: ${JSON.stringify(x + (y * 1000))}`);
            locations.push(<Location type={locationType} id={y} key={x + (y * 1000)} />);
          }
        }
        locationRows.push(<div key={y} className="row"> { locations } </div>);
        locations = [];
      }
    }

    const instructionStyle = {
      color: 'white',
      fontSize: '1em',
      float: 'left',
      maxWidth: '150px',
      position: 'absolute',
      marginTop: '50px',
      marginLeft: '8px',
    };
    return (
      <div className="main">
        <NotificationContainer />
        <div>
          <Container fluid className="mui-container">
            <Button
              variant="raised"
              className="btn btn-primary toggle"
              onClick={this.handleMyToggleDarkness}
            >
              Toggle Darkness
            </Button>

            <div style={instructionStyle}>

              <h3> Instructions: </h3>
              <dl>
                <dt>Move and Attack:</dt>
                <dd>• Use arrow keys: ← ↓ ↑ →</dd>
                <dd>• Or the letters: A S W D</dd>
                <dt>Win:</dt>
                <dd>• Win the game by defeating the Boss</dd>
                <dt>Lose:</dt>
                <dd>• Lose the game by running out of health</dd>
              </dl>
            </div>

          </Container>

          <div className="grid" >
            { locationRows }
          </div>
        </div>
        {
          //  `darkness: ${this.props.darkness}`
        }
      </div>
    );
  }
}

Dungeon.propTypes = {
  handleToggleDarkness: PropTypes.func.isRequired,
  darkness: PropTypes.bool.isRequired,
};

export default Dungeon;
