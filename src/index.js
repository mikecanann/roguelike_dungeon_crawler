import React from 'react';
import ReactDOM from 'react-dom';
import Dungeon from './Dungeon';

import { createStore } from 'redux';
import { Provider, connect } from 'react-redux';

// main.css is generated from main.scss
import './main.css';


// Action
const toggleDarknessAction = { type: 'toggleDarkness' };

// Action Creators
function movePlayer(keyPress) {
  return {
    type: 'movePlayer',
    keyPress,
  };
}

// bound action cretor
// const boundMovePlayer = (keyPress) => dispatch(movePlayer(keyPress))
//
// initial state:
const MAPSIZE = 60;
const startingState = {
  dungeonMap: [],
  rooms: [],
  cols: MAPSIZE,
  rows: MAPSIZE,
  player: {
    health: 100,
    level: 1,
    nextLevel: 20,
    experience: 1,
    damage: {
      min: 5,
      max: 10,
    },
  },
  boss: {
    health: 50,
    damage: {
      min: 20,
      max: 20,
    },
    experienceIncrease: 50,
  },
  darkness: true,
  enemies: [],
};


// Reducer
function mainReducer(state = startingState, action) {
  const count = state.count;
  switch (action.type) {
    case 'toggleDarkness':
      // console.log(`toggleDarkness from: ${state.darkness} reducer ${JSON.stringify(state)}`);
      return { ...state, darkness: !state.darkness };
    case 'movePlayer':
      // mrc - TODO not implemented yet
      // console.log(`mainReducer: action.type: ${action.type} keyPress: ${action.keyPress} `);
      return { count: count + 1 };
    default:
      return state;
  }
}

// Store
const store = createStore(mainReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

// Map Redux state to component props
function mapStateToProps(state) {
  return {
    darkness: state.darkness,
  };
}

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
  return {
    handlePlayerMove: (event) => dispatch(movePlayer(event)),
    handleToggleDarkness: () => dispatch(toggleDarknessAction),
  };
}

// Connected Component
const App = connect(
  mapStateToProps,
  mapDispatchToProps)(Dungeon);


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));

/*
ReactDOM.render(
  <Dungeon />,
  document.getElementById('root')
);

*/
