
import React, { PureComponent, PropTypes } from 'react';

import './App.css';
import './main.css';

class Location extends PureComponent {

  constructor(props) {
    super(props);

    this.state = {
      type: this.props.type,
    };
  }

  componentWillReceiveProps() {
    this.setState({
      type: this.props.type,
    });
  }

  render() {
    return (
      <div id={this.props.id}
        className={this.state.type}
      />
    );
  }
}

Location.propTypes = {
  type: PropTypes.string.isRequired,
};

export default Location;
